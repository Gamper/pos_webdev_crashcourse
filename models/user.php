<?php

class User
{
    private int $uid;
    private string $email;
    private string $pwhash;
    private string $pic;
    private static PDO $dbconnection;

    /**
     * Liefert die aktuelle UID des Users, wie sie auch in der DB-Tabelle genutzt wird
     * @return int Die eindeutige ID des Users
     */
    public function getUid(): int
    {
        return $this->uid;
    }

    /**
     * Setzt die UID des Users auf die UID in der DB-Tabelle
     * Wird beim ersten Erstellen des Users in der DB automatisch erstellt
     * @param int $uid Die eindeutige ID
     */
    private function setUid(int $uid)
    {
        $this->uid = $uid;
    }

    /**
     * Liefert die Email des Users
     * @return string Die Email als string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Setzt die Email-Adresse des Users auf einen neuen Wert.
     * @todo Email-Adresse sollte eindeutig sein. Prüfen, ob Wert in DB vorhanden
     * @param string $email Die neue Email
     * @return User Das aktualisierte User-Objekt
     * @throws Exception Ungültige Email-Adresse
     */
    public function setEmail(string $email): User
    {
        if (preg_match('/^(?=[a-z][a-z0-9@._-]{5,40}$)[a-z0-9._-]{1,20}@(?:(?=[a-z0-9-]{1,15}\.)[a-z0-9]+(?:-[a-z0-9]+)*\.){1,2}[a-z]{2,6}$/m',$email) === 1)
        {
            $this->email = $email;
            //echo  $email;
        }
        else{
           throw new Exception('Ungültige Email-Adresse');
        }
        return $this;
    }


    /**
     * Setzt das neue Passwort des Users
     * @param string $pw Das neue Passwort als Klartext. Muss Groß-/Kleinbuchstaben, Ziffern und Sonderzeichen enthalten und mindestens 8 Zeichen lang sein
     * @throws Exception Das gewählte Passwort entspricht nicht den Mindestanforderungen
     * */
    public function setPw(string $pw): User
    {
        if (preg_match('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W).{8,}$/m', $pw) === 1)
        {
            //echo "Starkes Passwort!";
            $this->pwhash= password_hash($pw,PASSWORD_DEFAULT);
        }
        else
        {
            //echo "Zu Schwach!";
            throw new Exception('Passwort zu schwach!');
        }
        return $this;
    }

    /**
     * Liefert den aktuellen PW-Hash
     * @return string der PW-Hash
     */
    private function getPwhash(): string
    {
        return $this->pwhash;
    }


    /**
     * Liefert das aktuelle Profilbild des Users
     * @return string die URL oder der Pfad zum Profilbild
     */
    public function getPic(): string
    {
        return $this->pic;
    }

    /**
     * Setzt für den User ein neues Profilbild
     * @param string $pic Der Pfad zum neuen Profilbild als relativer oder absoluter Pfad oder HTTP-URL
     * @return User
     * @todo Validieren der Bildurl
     */
    public function setPic(string $pic): User
    {
        $this->pic = $pic;
        return $this;
    }


    /**
     * Sichert das User-Objekt in die DB-Tabelle.
     * Die Methode entscheidet selbstständig an Hand der Email-Adresse,
     * ob der User bereits existiert oder ob ein neuer User erstellt wird.
     * @return void
     * @throws Exception Das Speicher des Users ist fehlgeschlagen
     */
    public function save()
    {
        $dbconn = self::dbConnect();
        $query = 'SELECT * FROM kisscms_db.t_users WHERE uemail = :email';
        $ps = $dbconn->prepare($query);
        $ps->bindParam(':email', $this->email, PDO::PARAM_STR);
        $ps->execute();
        if ($ps === false)
        {
            throw new Exception('Speichern des Users fehlgeschlagen.');
        }
        else{
            if ($ps->rowCount() === 0)
            {
                //echo "Email noch nicht vorhanden -> Neuer User";
                $query = "INSERT INTO kisscms_db.t_users(uemail,upwhash) VALUES (:email,:pwhash)";
                $ps = $dbconn->prepare($query);
                $ps->bindParam(':email', $this->email, PDO::PARAM_STR);
                $ps->bindParam(':pwhash', $this->pwhash, PDO::PARAM_STR);
                $ps->execute();
            }
            else
            {
                //echo "Email schon vorhanden -> Bestehender User";
                throw new Exception('User bereits vorhanden', 1111);
            }
        }
    }


    /**
     * Öffnet eine DB-Verbindung zur Datenbank
     * @return PDO Die Verbindung zur Datenbank
     * @throws PDOException Falls die Verbindung sich nicht öffnen lässt.
     * @todo DB-Verbindung in Superklasse auslagern und Codeduplizierung vermeiden
     */
    public static function dbConnect(): PDO
    {
        $config = require_once('../db/config.php');
        $db = $config['dbname'];
        $dbuser = $config['dbuser'];
        $dbpw = $config['dbpw'];
        $dbhost = $config['dbhost'];
        $dsn = "mysql:dbname=$db;host=$dbhost";
        try {
            self::$dbconnection = new PDO($dsn, $dbuser, $dbpw);
            //echo "Verbunden.";
            return self::$dbconnection;
        }
        catch(PDOException $e)
        {
            //echo "Verbindung fehlgeschlagen";
            throw $e;
        }
    }

}
