<?php

/**
 * Die Klasse beschreibt einen CMS-Artikel
 * Der DB-Zugriff auf die Tabelle wird mit dem ORM-Pattern ActiveRecord umgesetzt.
 * @author Michael Gamper
 * @version 02_03_2022
 */
class Artikel
{
    private int $aid;
    private string $atitel;
    private string $ainhalt;
    private string $abild;
    private int $fk_uid;
    private static PDO $dbconnection;

    /**
     * @param string $atitel Der Titel des Artikels
     * @param string $ainhalt Der Inhalt des Artikels
     * @param string|null $abild Das Artikelbild
     * @param int $aid Die eindeutige ID des Artikels. -1 falls noch nicht bekannt.
     * @param int $fk_uid Die Id des Autors (Fremdschlüssel). -1 Falls noch nicht bekannt.
     */
    public function __construct(string $atitel, string $ainhalt, string $abild=null,  int $aid=-1, int $fk_uid=-1)
    {
        $this->setAid($aid);
        $this->setAtitel($atitel);
        $this->setAinhalt($ainhalt);
        $this->setAbild($abild);
        $this->setFkUid($fk_uid);
    }


    /**
     * Retourniert den Titel des Artikels
     * @return string Der Titel des Artikels
     */
    public function getAtitel() :string
    {
        return $this->atitel;
    }


    /**
     * Setzt den Titel des Artikels auf einen neuen Wert
     * @param string $atitel Der neue Titel des Artikels
     * @return Artikel Das Artikel-Objekt
     */
    public function setAtitel(string $atitel): Artikel
    {
        $this->atitel = $atitel;
        return $this;
    }


    /**
     * Retourniert die ID des Artikels
     * @return int
     */
    public function getAid(): int
    {
        return $this->aid;
    }


    /**
     * Setzt die eindeutige ID des Artikels auf den Wert aus der DB
     * @param int $aid
     * @return Artikel
     */
    private function setAid(int $aid): Artikel
    {
        $this->aid = $aid;
        return $this;
    }


    /**
     * Liefert den Inhalt des Artikels
     * @return string Der Inhalt
     */
    public function getAinhalt(): string
    {
        return $this->ainhalt;
    }


    /**
     * Setzt den Inhalt des Artikels
     * @param string $ainhalt
     * @return Artikel
     */
    public function setAinhalt(string $ainhalt): Artikel
    {
        $this->ainhalt = $ainhalt;
        return $this;
    }


    /**
     * Retourniert die URL zum Artikelbild
     * @return string Die Bildurl
     */
    public function getAbild(): string
    {
        return $this->abild;
    }


    /**
     * Setzt ein neues Artikelbild
     * @param string $abild die neue URL als absoluter oder relativer Pfad
     * @return Artikel
     */
    public function setAbild(string $abild): Artikel
    {
        $this->abild = $abild;
        return $this;
    }


    /**
     * Retourniert die ID des Autors des Artikels
     * @return int die ID des Autors
     * @todo eventuell gleich in der Usertabelle nachschlagen und ein Userobjekt zurückliefern.
     */
    public function getFkUid(): int
    {
        return $this->fk_uid;
    }


    /**
     * Weist dem Artikel einen neuen Autor zu
     * @param mixed $fk_uid Die ID des Users, der dem Artikel zugewiesen werden soll
     * @return Artikel
     */
    public function setFkUid(int $fk_uid): Artikel
    {
        $this->fk_uid = $fk_uid;
        return $this;
    }


    /**
     * Die Methode liest den Artikel mit der gesuchten ID aus der Datenbank und gibt ein Artikel-Objekt zurück.
     * @param int $id Die ID des gesuchten Artikels als Ganzzahl
     * @return Artikel|false Retourniert den Artikel aus der Datenbank oder boolean false, falls kein Artikel gefunden wurde.
     * @throws Exception Falls ein DB-Zugriffsfehler auftreten sollte, wird diese Exception geworfen.
     */
    public static function getArtikelByID(int $id): Artikel|false
    {
        try {
            $pdoconn = self::dbConnect();
            //$pdoconn = new PDO($dsn, $dbuser, $dbpw);
            //echo "Verbunden.";
            $query = "SELECT * FROM kisscms_db.t_articles WHERE aid=:id;";
            $ps = $pdoconn->prepare($query);
            if ($ps === false)
            {
                echo 'Prepeared Statement fehlgeschlagen.';
                throw new Exception('Prepeared Statement fehlgeschlagen');
            }
            else
            {
                $psresult = $ps->bindParam(':id', $id, PDO::PARAM_INT);
                $psresult = $ps->execute();
                if ($psresult === false)
                {
                    echo 'Binden der Variable fehlgeschlagen.';
                    throw new Exception('DB-Zugriff: Binden der Variable fehlgeschlagen.',1);
                }
                else
                {
                    //echo 'Träumchen: PS erfolgreich';
                    $result = $ps->fetchObject();
                    if ($result === false)
                    {
                        return false;
                    }
                    else{
                        //var_dump($result);
                        $artikel = new Artikel($result->atitle, $result->acontent, $result->apic, $result->aid, -1);
                        //$artikel->setAid($result->aid);
                        //$artikel->setAtitel($result->atitle);
                        //$artikel->setAinhalt($result->acontent);
                        //$artikel->setAbild($result->apic);
                        return $artikel;
                    }
                }
            }
        }
        catch(PDOException $e)
        {
            echo 'Fehler'.$e->getMessage();
            throw $e;
        }
    }


    /**
     * Öffnet eine DB-Verbindung zur Datenbank
     * @return PDO
     * @throws PDOException Falls die Verbindung sich nicht öffnen lässt.
     */
    public static function dbConnect(): PDO
    {
        $config = require_once('../db/config.php');
        $db = $config['dbname'];
        $dbuser = $config['dbuser'];
        $dbpw = $config['dbpw'];
        $dbhost = $config['dbhost'];
        $dsn = "mysql:dbname=$db;host=$dbhost";
        try {
            self::$dbconnection = new PDO($dsn, $dbuser, $dbpw);
            //echo "Verbunden.";
            return self::$dbconnection;
        }
        catch(PDOException $e)
        {
            echo "Verbindung fehlgeschlagen";
            throw $e;
        }
    }

} // Ende Klasse Artikel
