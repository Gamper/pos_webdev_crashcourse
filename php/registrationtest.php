<?php

require('../models/user.php');

$email = filter_input(INPUT_POST,'email', FILTER_VALIDATE_EMAIL);
$pw =  $_POST['password'];

try {
    $user = new User();
    $user->setEmail($email);
    $user->setPw($pw);
    $user->save();
}
catch (Exception $e)
{
    echo $e->getMessage();
}

