<?php

//Inkludiere die benötigte Klasse Artikel
require_once('../models/artikel.php');

try {
    //Hole einen neuen Artikel über die statische Methode der Klasse Artikel
    $artikel = Artikel::getArtikelByID(2);
    echo 'Titel des Artikels: '.$artikel->getAtitel();
}
catch (Exception $e)
{
    echo 'Fehler: '.$e->getMessage();
}

