<?php
/**
 * Exemplarischer DB-Zugriff mit PDO
 * @todo Löschen
 */

$config = require_once('./config.php');
$db = $config['dbname'];
$dbuser = $config['dbuser'];
$dbpw = $config['dbpw'];
$dbhost = $config['dbhost'];

$dsn = "mysql:dbname=$db;host=$dbhost";


try {
    $pdoconn = new PDO($dsn,$dbuser,$dbpw);
    echo "Alles fein!";
}
catch (PDOException $e){
    echo $e;
}


