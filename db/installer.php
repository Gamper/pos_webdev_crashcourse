<?php
/**
 * Erstelle die Datenbank, die Tabellen und die Testdaten.
 */

$config = require_once('./config.php');
$db = $config['dbname'];
$dbuser = $config['dbuser'];
$dbpw = $config['dbpw'];
$dbhost = $config['dbhost'];

//$dsn = "mysql:dbname=$db;host=$dbhost";
$dsn = "mysql:host=$dbhost";

try {
    $pdoconn = new PDO($dsn,$dbuser,$dbpw);

    echo "Verbunden. Import startet...";

    $sql = file_get_contents('kisscms_db.sql');

    $qr = $pdoconn->exec($sql);
    if ($qr === false or $sql === false)
    {
        echo  "Import fehlgeschlagen!";
    }
    else
    {
        echo "Import erfolgreich!";
    }
}
catch (PDOException $e){
    echo "DB-Fehler: ".$e->getMessage();
}
